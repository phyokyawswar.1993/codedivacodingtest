//
//  BaseViewController.swift
//  CodedivaCodeTest
//
//  Created by Phyo Kyaw Swar on 26/06/2021.
//

import UIKit
import Combine
import CombineCocoa
class BaseViewController: UIViewController {
    var bindings = Set<AnyCancellable>()
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.bindData()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AppScreen.shared.currentVC = self
        addBackButton()
    }
    
    func setupUI() {
        
    }
    
    func bindData() {
        
    }
    
    func hideNavigationBar(isHide : Bool) {
        self.navigationController?.setNavigationBarHidden(isHide, animated: true)
    }
    
    
    func addBackButton() {
        
        let backBtn = UIButton(type: .custom)
        backBtn.setBackgroundImage(#imageLiteral(resourceName: "icons8-left-48"), for: .normal)
        
            backBtn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        
        
        backBtn.addTarget(self, action: #selector(didTapBackBtn), for: .touchUpInside)
        backBtn.contentMode = .scaleAspectFit
        backBtn.tintColor = UIColor.white
        let backBarBtnItem = UIBarButtonItem(customView: backBtn)
       
        navigationItem.leftBarButtonItem = backBarBtnItem
        
    }
    
    @objc func didTapBackBtn() {
        
        navigationController?.popViewController(animated: true)
        
    }

}
