//
//  LoginViewModel.swift
//  CodedivaCodeTest
//
//  Created by Phyo Kyaw Swar on 26/06/2021.
//

import Foundation
import Combine

class LoginViewModel {
    let userNamePublisher = CurrentValueSubject<String?,Never>(nil)
    let passworPublisher = CurrentValueSubject<String?,Never>(nil)
}

extension LoginViewModel {
    func isEnableLoginBtn() -> AnyPublisher<Bool?,Never> {
        return Publishers.CombineLatest(userNamePublisher, passworPublisher).compactMap { (username , password) -> Bool? in
            return !(username?.isEmpty ?? true) && (username?.count ?? 0) >= 4 && !(password?.isEmpty ?? true) && (password?.count ?? 0) >= 4
        }.eraseToAnyPublisher()
    }
}
