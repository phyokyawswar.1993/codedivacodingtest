//
//  PinCodeViewModel.swift
//  CodedivaCodeTest
//
//  Created by Phyo Kyaw Swar on 27/06/2021.
//

import Foundation
import Combine
class PinCodeViewModel {
    let pinOnePublisher = CurrentValueSubject<Int?,Never>(nil)
    let pinTwoPublisher = CurrentValueSubject<Int?,Never>(nil)
    let pinThreePublisher = CurrentValueSubject<Int?,Never>(nil)
    let pinFourPublisher = CurrentValueSubject<Int?,Never>(nil)
    let pinFivePublisher = CurrentValueSubject<Int?,Never>(nil)
    let pinSixPublisher = CurrentValueSubject<Int?,Never>(nil)
    
    let confirmPinOnePublisher = CurrentValueSubject<Int?,Never>(nil)
    let confirmPinTwoPublisher = CurrentValueSubject<Int?,Never>(nil)
    let confirmPinThreePublisher = CurrentValueSubject<Int?,Never>(nil)
    let confirmPinFourPublisher = CurrentValueSubject<Int?,Never>(nil)
    let confirmPinFivePublisher = CurrentValueSubject<Int?,Never>(nil)
    let confirmPinSixPublisher = CurrentValueSubject<Int?,Never>(nil)
}
