//
//  RequestOTPViewController.swift
//  CodedivaCodeTest
//
//  Created by Phyo Kyaw Swar on 26/06/2021.
//

import UIKit

class RequestOTPViewController: BaseViewController {

    @IBOutlet weak var imgOTPIcon: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var btnRequest: UIButton!
    @IBOutlet weak var lblContact: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        hideNavigationBar(isHide: false)
    }
    

    override func setupUI() {
        super.setupUI()
        lblTitle.text = "OTP will be sent to the number"
        lblTitle.font  = .titleFont
        lblTitle.textColor = .lightGray
        
        lblPhone.text = "082-xxx-8998"
        lblPhone.font = .largeBoldFont
        lblPhone.textColor = .appColor
        
        btnRequest.setTitle("Request OTP", for: .normal)
        btnRequest.titleLabel?.font = .titleFont
        
        lblContact.text = "Case the phone number is not correct . Please contact 02-xxx-xxxx"
        lblContact.font = .smallNormalFont
        lblContact.textColor = .lightGray
        
        imgOTPIcon.image = #imageLiteral(resourceName: "OTP_ic")
    }
    
    override func bindData() {
        super.bindData()
        
        btnRequest.tapPublisher.sink {_ in
            AppScreen.shared.navigateToOTPVerifyVC()
        }.store(in: &bindings)
    }
    
    

}
