//
//  SelectedLanguageViewController.swift
//  CodedivaCodeTest
//
//  Created by Phyo Kyaw Swar on 26/06/2021.
//

import UIKit

class SelectedLanguageViewController: BaseViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    @IBOutlet weak var btnEng: UIButton!
    @IBOutlet weak var btnThai: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func setupUI() {
        super.setupUI()
        lblTitle.font = .titleFont
        lblTitle.text = "Welcome"
        lblSubtitle.font = .normalFont
        lblSubtitle.text = "Please select a language"
        
        btnEng.setTitle("Eng", for: .normal)
        btnEng.backgroundColor = .appColor
        btnEng.setTitleColor(.white, for: .normal)
        
        btnThai.setTitle("Thai", for: .normal)
        btnThai.backgroundColor = .appColor
        btnThai.setTitleColor(.white, for: .normal)
      
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.hideNavigationBar(isHide: true)
    }
    
    override func bindData() {
        super.bindData()
        
        // button tap event with combine
        btnEng.tapPublisher.sink { _ in
            AppScreen.shared.presentTermsAndCondition()
        }.store(in : &bindings)
        
        btnThai.tapPublisher.sink { _ in
            AppScreen.shared.presentTermsAndCondition()
        }.store(in: &bindings)
    }


}
