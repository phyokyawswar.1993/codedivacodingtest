//
//  TermsAndConditionViewController.swift
//  CodedivaCodeTest
//
//  Created by Phyo Kyaw Swar on 26/06/2021.
//

import UIKit

class TermsAndConditionViewController: BaseViewController {

    @IBOutlet weak var btnDenied: RoundedCornerUIButton!
    @IBOutlet weak var btnAccept: RoundedCornerUIButton!
    @IBOutlet weak var lblTerms: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "Terms of service"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func setupUI() {
        super.setupUI()
        btnDenied.setTitle("Denied", for: .normal)
        btnDenied.setTitleColor(.appColor, for: .normal)
        
        btnAccept.setTitle("Accept", for: .normal)
        btnAccept.setTitleColor(.white, for: .normal)
        
        lblTerms.text = ""
//        let view = NavigationTitleView(frame: self.navigationItem.titleView?.frame ?? CGRect.zero)
//        view.lblTitle.text = "Terms of service"
    }
    
    override func bindData() {
        super.bindData()
        
        btnAccept.tapPublisher.sink {_ in
            self.dismiss(animated: true, completion: nil)
            AppScreen.shared.navigateToLoginVC()
        }.store(in: &bindings)
        
        btnDenied.tapPublisher.sink { _ in
            self.dismiss(animated: true, completion: nil)
        }.store(in: &bindings)
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
