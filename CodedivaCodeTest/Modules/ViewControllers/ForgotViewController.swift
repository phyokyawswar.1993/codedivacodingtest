//
//  ForgotViewController.swift
//  CodedivaCodeTest
//
//  Created by Phyo Kyaw Swar on 26/06/2021.
//

import UIKit

class ForgotViewController: BaseViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblRegister: UILabel!
    @IBOutlet weak var btnSend: RoundedCornerUIButton!
    @IBOutlet weak var txtEmailPhone: UITextField!
    var viewModel = ForgotPasswordViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        hideNavigationBar(isHide: false)
        self.isEnableSendBtn(isEnable: false)
    }
    
    override func bindData() {
        super.bindData()
        
        btnSend.tapPublisher.sink { _ in
            AppScreen.shared.navigateToSuccessVC()
        }.store(in: &bindings)
        
        txtEmailPhone.textPublisher.sink {
            self.viewModel.emailOrPhonePublisher.send($0)
        }.store(in: &bindings)
        
        viewModel.emailOrPhonePublisher.sink {
            if let _ = $0 {
                self.isEnableSendBtn(isEnable: true)
            }
            else {
                self.isEnableSendBtn(isEnable: false)
            }
        }.store(in: &bindings)
        
    }
    override func setupUI() {
        super.setupUI()
        lblTitle.text = "Forgot password"
        lblTitle.textColor = .black
        lblTitle.font = .largeBoldFont
        
        lblDesc.text = "Please enter your email or phone number."
        lblDesc.textColor = .lightGray
        lblDesc.font = .normalFont
        
        lblRegister.text = ""
        lblRegister.textColor = .lightGray
        lblRegister.font = .normalFont
        
        txtEmailPhone.placeholder = "Email / Phone"
        
        btnSend.setTitle("Send", for: .normal)
        btnSend.titleLabel?.font = .titleFont
        
    }
    
    private func isEnableSendBtn (isEnable : Bool ) {
        btnSend.isUserInteractionEnabled = isEnable
        btnSend.alpha = isEnable ? 1.0 : 0.8
    }
}
