//
//  VerifyOTPViewController.swift
//  CodedivaCodeTest
//
//  Created by Phyo Kyaw Swar on 26/06/2021.
//

import UIKit
import OTPFieldView
class VerifyOTPViewController: BaseViewController {
    @IBOutlet weak var otpView: OTPFieldView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblDonotReceive: UILabel!
    @IBOutlet weak var lblResend: UILabel!
    @IBOutlet weak var lblTimer: UILabel!
    
    var otpResendCounter = 60
    var timer = Timer()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func setupUI() {
        super.setupUI()
        setupOtpView()
        lblTitle.text = "Confirmation identity"
        lblTitle.font = .titleFont
        lblTitle.textColor = .black
       
        lblDesc.text = "Please enter the verification code we have sent to you"
        lblDesc.font = .normalFont
        lblDesc.textColor = .lightGray
        
        lblPhone.text = "082-xxx-8998"
        lblPhone.font = .normalFont
        lblPhone.textColor = .lightGray
       
        lblDonotReceive.text = "If you didn't recieve the code?"
        lblDonotReceive.font = .normalFont
        lblDonotReceive.textColor = .lightGray
        
        lblResend.text = "Recent the code"
        lblResend.font = .normalFont
        lblResend.textColor = .darkGray
        
        lblTimer.text = ""
        lblTimer.textColor = .appColor
        lblTimer.font = .normalFont
        
        setupCounter()
        
    }
    func setupOtpView(){
            self.otpView.fieldsCount = 6
            self.otpView.fieldBorderWidth = 1
            self.otpView.defaultBorderColor = UIColor.lightGray
            self.otpView.filledBorderColor = UIColor.green
            self.otpView.cursorColor = UIColor.red
            self.otpView.displayType = .underlinedBottom
            self.otpView.fieldSize = 40
            self.otpView.separatorSpace = 8
            self.otpView.shouldAllowIntermediateEditing = false
            self.otpView.delegate = self
            self.otpView.initializeUI()
        }

    func setupCounter() {
        var defaultTime = 60
        Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { timer in
            defaultTime -= 1
            self.lblTimer.text = "(\(defaultTime))"
            if defaultTime == 0 {
                self.lblTimer.text = ""
                self.setupLblResetnEnable(isEnable: true)
                timer.invalidate()
            }
        }
    }
    
    func setupLblResetnEnable(isEnable : Bool) {
        lblResend.isUserInteractionEnabled = isEnable
        lblResend.textColor = isEnable ? .link : .darkGray
    }

}

extension VerifyOTPViewController : OTPFieldViewDelegate {
    func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
       return true
    }
    
    func enteredOTP(otp: String) {
        
    }
    
    func hasEnteredAllOTP(hasEnteredAll: Bool) -> Bool {
        if hasEnteredAll {
            AppScreen.shared.navigateToPinCodeVC()
        }
        
        return hasEnteredAll
    }
}
