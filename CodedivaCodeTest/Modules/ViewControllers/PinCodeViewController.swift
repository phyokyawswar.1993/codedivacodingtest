//
//  PinCodeViewController.swift
//  CodedivaCodeTest
//
//  Created by Phyo Kyaw Swar on 27/06/2021.
//

import UIKit

class PinCodeViewController: BaseViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgViewOne: RoundedUIImageView!
    @IBOutlet weak var imgViewTwo: RoundedUIImageView!
    @IBOutlet weak var imgViewThree: RoundedUIImageView!
    @IBOutlet weak var imgViewFour: RoundedUIImageView!
    
    @IBOutlet weak var imgViewFive: RoundedUIImageView!
    @IBOutlet weak var imgVeiwSix: RoundedUIImageView!
    @IBOutlet weak var numberCollection: UICollectionView!
    
    var viewModel = PinCodeViewModel()
    var isConfirmPin = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func setupUI() {
        super.setupUI()
        setupCollectionView()
        lblTitle.font = .titleFont
        lblTitle.text = "Set the PIN Code"
    }
    
    override func bindData() {
        super.bindData()
        
        viewModel.pinOnePublisher.sink {
            if let _ = $0 {
                self.imgViewOne.backgroundColor = .appColor
            }
            else {
                self.imgViewOne.backgroundColor = .white
            }
        }.store(in: &bindings)
        
        viewModel.pinTwoPublisher.sink {
            if let _ = $0 {
                self.imgViewTwo.backgroundColor = .appColor
            }
            else {
                self.imgViewTwo.backgroundColor = .white
            }
        }.store(in: &bindings)
        
        viewModel.pinThreePublisher.sink {
            if let _ = $0 {
                self.imgViewThree.backgroundColor = .appColor
            }
            else {
                self.imgViewThree.backgroundColor = .white
            }
        }.store(in: &bindings)
        
        viewModel.pinFourPublisher.sink {
            if let _ = $0 {
                self.imgViewFour.backgroundColor = .appColor
            }
            else {
                self.imgViewFour.backgroundColor = .white
            }
        }.store(in: &bindings)
        
        viewModel.pinFivePublisher.sink {
            if let _ = $0 {
                self.imgViewFive.backgroundColor = .appColor
            }
            else {
                self.imgViewFive.backgroundColor = .white
            }
        }.store(in: &bindings)
        
        viewModel.pinSixPublisher.sink {
            if let _ = $0 {
                self.imgVeiwSix.backgroundColor = .appColor
                self.isConfirmPin = true
                self.clearPinView()
                self.numberCollection.reloadItems(at: [IndexPath(item: 9, section: 0)])
            }
            else {
                self.imgVeiwSix.backgroundColor = .white
            }
        }.store(in: &bindings)
        
        viewModel.confirmPinOnePublisher.sink {
            if let _ = $0 {
                self.imgViewOne.backgroundColor = .appColor
            }
            else {
                self.imgViewOne.backgroundColor = .white
            }
        }.store(in: &bindings)
        
        viewModel.confirmPinTwoPublisher.sink {
            if let _ = $0 {
                self.imgViewTwo.backgroundColor = .appColor
            }
            else {
                self.imgViewTwo.backgroundColor = .white
            }
        }.store(in: &bindings)
        
        viewModel.confirmPinThreePublisher.sink {
            if let _ = $0 {
                self.imgViewThree.backgroundColor = .appColor
            }
            else {
                self.imgViewThree.backgroundColor = .white
            }
        }.store(in: &bindings)
        
        viewModel.confirmPinFourPublisher.sink {
            if let _ = $0 {
                self.imgViewFour.backgroundColor = .appColor
            }
            else {
                self.imgViewFour.backgroundColor = .white
            }
        }.store(in: &bindings)
        
        viewModel.confirmPinFivePublisher.sink {
            if let _ = $0 {
                self.imgViewFive.backgroundColor = .appColor
            }
            else {
                self.imgViewFive.backgroundColor = .white
            }
        }.store(in: &bindings)
        
        viewModel.confirmPinSixPublisher.sink {
            if let _ = $0 {
                self.imgVeiwSix.backgroundColor = .appColor
                
            }
            else {
                self.imgVeiwSix.backgroundColor = .white
            }
        }.store(in: &bindings)
        
    }
    
    private func setupCollectionView() {
        
        numberCollection.register(UINib(nibName: String(describing: NumberPadCollectionViewCell.self), bundle: nil), forCellWithReuseIdentifier: "NumberPadCollectionViewCell")
        numberCollection.delegate = self
        numberCollection.dataSource = self
        numberCollection.reloadData()
    }
    
    func clearPinView() {
        imgViewOne.backgroundColor = .white
        imgViewTwo.backgroundColor = .white
        imgViewThree.backgroundColor = .white
        imgViewFour.backgroundColor = .white
        imgViewFive.backgroundColor = .white
        imgVeiwSix.backgroundColor = .white
        
    }
    
}

extension PinCodeViewController : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 12
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"NumberPadCollectionViewCell", for: indexPath) as! NumberPadCollectionViewCell
        if indexPath.item < 9 {
            cell.setupCell(title : "\(indexPath.row + 1)", indexPath: indexPath)
        }
        else if indexPath.item == 10 {
            cell.setupCell(title : "0" , indexPath: indexPath)
        }
        else if indexPath.item == 9 {
            if isConfirmPin {
                cell.setupCellwithImage(image: #imageLiteral(resourceName: "icons8-fingerprint-50") , indexPath: indexPath)
            }
        }
        else if indexPath.item == 11{
            cell.setupCellwithImage(image: #imageLiteral(resourceName: "ic_delete") , indexPath: indexPath)
        }
        cell.delegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.size.width / 3
        return CGSize(width: width , height: width)
    }
    
    
}

extension PinCodeViewController : NumberPadCollectionViewCellDelegate {
    func didTapNumberBtn(indexPath: IndexPath) {
        if indexPath.item < 9 || indexPath.item == 10 {
            if !isConfirmPin {
                if viewModel.pinOnePublisher.value == nil {
                    viewModel.pinOnePublisher.send(indexPath.item)
                }
                else if viewModel.pinTwoPublisher.value == nil {
                    viewModel.pinTwoPublisher.send(indexPath.item)
                }
                else if viewModel.pinThreePublisher.value == nil {
                    viewModel.pinThreePublisher.send(indexPath.item)
                }
                else if viewModel.pinFourPublisher.value == nil {
                    viewModel.pinFourPublisher.send(indexPath.item)
                }
                else if viewModel.pinFivePublisher.value == nil {
                    viewModel.pinFivePublisher.send(indexPath.item)
                }
                else  {
                    viewModel.pinSixPublisher.send(indexPath.item)
                }
            }
            else {
                if viewModel.confirmPinOnePublisher.value == nil {
                    viewModel.confirmPinOnePublisher.send(indexPath.item)
                }
                else if viewModel.confirmPinTwoPublisher.value == nil {
                    viewModel.confirmPinTwoPublisher.send(indexPath.item)
                }
                else if viewModel.confirmPinThreePublisher.value == nil {
                    viewModel.confirmPinThreePublisher.send(indexPath.item)
                }
                else if viewModel.confirmPinFourPublisher.value == nil {
                    viewModel.confirmPinFourPublisher.send(indexPath.item)
                }
                else if viewModel.confirmPinFivePublisher.value == nil {
                    viewModel.confirmPinFivePublisher.send(indexPath.item)
                }
                else  {
                    viewModel.confirmPinSixPublisher.send(indexPath.item)
                }
            }
            
        }
        else if indexPath.item == 9 {
            AppScreen.shared.navigateToFingerPrinVC()
        }
        else {
            if !isConfirmPin {
                if let _ = viewModel.pinSixPublisher.value {
                    viewModel.pinSixPublisher.send(nil)
                }
                else if let _ = viewModel.pinFivePublisher.value {
                    viewModel.pinFivePublisher.send(nil)
                }
                else if let _ = viewModel.pinFourPublisher.value {
                    viewModel.pinFourPublisher.send(nil)
                }
                else if let _ = viewModel.pinThreePublisher.value {
                    viewModel.pinThreePublisher.send(nil)
                }
                else if let _ = viewModel.pinTwoPublisher.value {
                    viewModel.pinTwoPublisher.send(nil)
                }
                else {
                    viewModel.pinOnePublisher.send(nil)
                }
            }
            else {
                if let _ = viewModel.confirmPinSixPublisher.value {
                    viewModel.confirmPinSixPublisher.send(nil)
                }
                else if let _ = viewModel.confirmPinFivePublisher.value {
                    viewModel.confirmPinFivePublisher.send(nil)
                }
                else if let _ = viewModel.confirmPinFourPublisher.value {
                    viewModel.confirmPinFourPublisher.send(nil)
                }
                else if let _ = viewModel.confirmPinThreePublisher.value {
                    viewModel.confirmPinThreePublisher.send(nil)
                }
                else if let _ = viewModel.confirmPinTwoPublisher.value {
                    viewModel.confirmPinTwoPublisher.send(nil)
                }
                else {
                    viewModel.confirmPinOnePublisher.send(nil)
                }
            }
            
        }
    }
}
