//
//  FingerPrintViewController.swift
//  CodedivaCodeTest
//
//  Created by Phyo Kyaw Swar on 27/06/2021.
//

import UIKit

class FingerPrintViewController:
BaseViewController{
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var btnSetFingerPrint: RoundedCornerUIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    override func setupUI() {
        super.setupUI()
        lblTitle.text = "Touch ID"
        lblTitle.font = .titleFont
        
        lblSubTitle.text = "Set lock with finger skin"
        lblSubTitle.font = .normalFont
        lblSubTitle.textColor = .lightGray
        
        lblDesc.text = "To access faster"
        lblDesc.font = .normalFont
        lblDesc.textColor = .lightGray
        
        btnSetFingerPrint.setTitle("Setup finger print", for: .normal)
        btnSetFingerPrint.titleLabel?.font = .titleFont
        
        btnCancel.setTitle("Cancel", for: .normal)
        btnCancel.setTitleColor(.appColor, for: .normal)
        btnCancel.titleLabel?.font = .titleFont
    }
    
    override func bindData() {
        super.bindData()
        btnCancel.tapPublisher.sink { _ in
            self.showAlert()
            
        }.store(in: &bindings)
    }
    
    func showAlert() {
        let alert = UIAlertController(title: "Touch ID for\n \"CGS Application\"", message: "Access with Touch ID or Cancel \n to go back to use PIN Code", preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
            self.dismiss(animated: true, completion: nil)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
       
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
        
    }
    
}
