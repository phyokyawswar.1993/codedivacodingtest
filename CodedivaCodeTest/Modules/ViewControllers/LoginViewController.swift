//
//  LoginViewController.swift
//  CodedivaCodeTest
//
//  Created by Phyo Kyaw Swar on 26/06/2021.
//

import UIKit

class LoginViewController: BaseViewController {

    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnCheck: UIButton!
    @IBOutlet weak var lblSaveLogin: UILabel!
    @IBOutlet weak var btnForgotPassword: UIButton!
    @IBOutlet weak var btnLogin: RoundedCornerUIButton!
    @IBOutlet weak var lblDonnotHaveAccount: UILabel!
    @IBOutlet weak var btnRegister: RoundedCornerUIButton!
    
    var viewModel = LoginViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        hideNavigationBar(isHide: true)
    }
    
    override func setupUI() {
        super.setupUI()
        txtUsername.placeholder = "User name"
        txtPassword.placeholder = "Password"
        lblSaveLogin.font = .normalFont
        lblDonnotHaveAccount.font = .normalFont
        btnLogin.titleLabel?.font = .titleFont
        btnRegister.titleLabel?.font = .titleFont
    }
    
    override func bindData() {
        super.bindData()
        
        btnLogin.tapPublisher.sink { _ in
            AppScreen.shared.navigateToRequestOTPVC()
        }.store(in: &bindings)
        
        btnForgotPassword.tapPublisher.sink {_ in
            AppScreen.shared.navigateToForgotPassVC()
        }.store(in: &bindings)
        
        viewModel.isEnableLoginBtn().sink {
            self.isEnableLoginBtn(isEnable: $0 ?? false)
        }.store(in: &bindings)
        
        txtUsername.textPublisher.sink {
            self.viewModel.userNamePublisher.send($0)
        }.store(in: &bindings)
        
        txtPassword.textPublisher.sink {
            self.viewModel.passworPublisher.send($0)
        }.store(in: &bindings)
    }
    
    private func isEnableLoginBtn(isEnable : Bool) {
        btnLogin.isUserInteractionEnabled = isEnable
        btnLogin.alpha = isEnable ? 1.0 : 0.5
    }

}
