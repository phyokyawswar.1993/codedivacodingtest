//
//  ResetSuccessViewController.swift
//  CodedivaCodeTest
//
//  Created by Phyo Kyaw Swar on 27/06/2021.
//

import UIKit

class ResetSuccessViewController: BaseViewController {

    @IBOutlet weak var imgView: RoundedUIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    @IBOutlet weak var btnOK: RoundedCornerUIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func bindData() {
        super.bindData()
        btnOK.tapPublisher.sink { _ in
            AppScreen.shared.goBackToLoginVC()
        }.store(in: &bindings)
    }
    override func setupUI() {
        super.setupUI()
        lblTitle.text = "Success"
        lblTitle.font = .titleFont
        
        lblSubtitle.text = "Reset your password successfully"
        lblSubtitle.font = .normalFont
        lblSubtitle.textColor = .lightGray
        
        btnOK.setTitle("OK", for: .normal)
        btnOK.titleLabel?.font = .titleFont
    }
  
}
