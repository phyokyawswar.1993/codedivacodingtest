//
//  ViewController.swift
//  CodedivaCodeTest
//
//  Created by Phyo Kyaw Swar on 26/06/2021.
//

import UIKit

class SplashViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.view.backgroundColor = .appColor
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.5) {
            self.gotoNavigationController()
        }
    }
    
    private func gotoNavigationController(){
        let window = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        let initialViewController = SelectedLanguageViewController.init()
        let navigationVC = UINavigationController(rootViewController : initialViewController)
        window!.rootViewController = navigationVC
        window!.makeKeyAndVisible()
        
    }

}

