//
//  NumberPadCollectionViewCell.swift
//  CodedivaCodeTest
//
//  Created by Phyo Kyaw Swar on 27/06/2021.
//

import UIKit
import Combine
protocol NumberPadCollectionViewCellDelegate {
    func didTapNumberBtn(indexPath : IndexPath)
}
class NumberPadCollectionViewCell: UICollectionViewCell {

    var bindings = Set<AnyCancellable>()
    @IBOutlet weak var btnNumberPad: RoundedCornerUIButton!
    
    var delegate : NumberPadCollectionViewCellDelegate?
    var indexPath = IndexPath()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        btnNumberPad.setTitle("", for: .normal)
        btnNumberPad.borderWidth = 0
        bindData()
    }
    
    func bindData() {
        btnNumberPad.tapPublisher.sink {_ in
            
            self.delegate?.didTapNumberBtn(indexPath: self.indexPath)
        }.store(in: &bindings)
    }
    
    func setupCell(title : String , isShowBorder : Bool = true , image : UIImage? = nil , indexPath : IndexPath) {
        self.indexPath = indexPath
        if let img = image {
            btnNumberPad.setImage(img, for: .normal)
        }
        else {
            btnNumberPad.setTitle(title, for: .normal)
        }
        
        btnNumberPad.borderWidth = isShowBorder ? 1 : 0
    }
    
    func setupCellwithImage(image : UIImage , indexPath : IndexPath) {
        self.indexPath = indexPath
        btnNumberPad.setImage(image, for: .normal)
        btnNumberPad.setTitle("", for: .normal)
        btnNumberPad.borderWidth = 0
    }

}
