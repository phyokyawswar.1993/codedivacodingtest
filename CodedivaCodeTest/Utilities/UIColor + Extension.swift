//
//  UIColor + Extension.swift
//  CodedivaCodeTest
//
//  Created by Phyo Kyaw Swar on 26/06/2021.
//

import Foundation
import UIKit

extension UIColor {
    class var appColor : UIColor {
        return #colorLiteral(red: 0.8980392157, green: 0.4745098039, blue: 0.5058823529, alpha: 1)
    }
}
