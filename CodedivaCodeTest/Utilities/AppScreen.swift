//
//  AppScreen.swift
//  CodedivaCodeTest
//
//  Created by Phyo Kyaw Swar on 26/06/2021.
//

import Foundation
import UIKit
class AppScreen {
    private init() {}
    
    static let shared = AppScreen()
    
    var currentVC : UIViewController?
}
extension AppScreen {
    func presentTermsAndCondition() {
        let vc = TermsAndConditionViewController.init()
        let navVC = UINavigationController(rootViewController: vc)
        currentVC?.present(navVC, animated: true, completion: nil)
    }
    
    func navigateToLoginVC() {
        let vc = LoginViewController.init()
        currentVC?.navigationController?.pushViewController(vc, animated: true)
    }
    
    func navigateToRequestOTPVC() {
        let vc = RequestOTPViewController.init()
        currentVC?.navigationController?.pushViewController(vc, animated: true)
    }
    
    func navigateToOTPVerifyVC(){
        let vc = VerifyOTPViewController.init()
        currentVC?.navigationController?.pushViewController(vc, animated: true)
    }
    
    func navigateToPinCodeVC() {
        let vc = PinCodeViewController.init()
        currentVC?.navigationController?.pushViewController(vc, animated: true)
    }
    
    func navigateToFingerPrinVC() {
        let vc = FingerPrintViewController.init()
        currentVC?.present(vc, animated: true, completion: nil)
    }
    
    func navigateToForgotPassVC() {
        let vc = ForgotViewController.init()
        currentVC?.navigationController?.pushViewController(vc, animated: true)
    }
    
    func goBackToLoginVC() {
        if let destinationViewController = currentVC?.navigationController?.viewControllers.filter(
            {$0 is LoginViewController}).first as? LoginViewController {
           
            currentVC?.navigationController?.popToViewController(destinationViewController, animated: true)
        }
    }
    
    func navigateToSuccessVC() {
        let vc = ResetSuccessViewController.init()
        currentVC?.navigationController?.pushViewController(vc, animated: true)
    }
}
