//
//  UIFont + Extension.swift
//  CodedivaCodeTest
//
//  Created by Phyo Kyaw Swar on 26/06/2021.
//

import Foundation
import UIKit

extension UIFont {
    class var titleFont : UIFont {
        return UIFont(name : "Roboto-Bold" , size : 15.0 )!
    }
    
    class var normalFont : UIFont {
        return UIFont(name : "Roboto-Regular" , size : 13.0 )!
    }
    
    class var smallNormalFont : UIFont {
        return UIFont(name : "Roboto-Regular" , size : 11.0 )!
    }
    
    class var largeBoldFont : UIFont {
        return UIFont(name : "Roboto-Bold" , size : 18.0 )!
    }
}
